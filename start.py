
import os
from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello World!'

if __name__ == "__main__":
    port = int(os.environ.get("WEB_PORT",5000))
    app.run(host=os.environ.get("IP_ADDR","0.0.0.0"), port=port)